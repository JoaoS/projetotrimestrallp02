object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 431
  ClientWidth = 751
  Color = clHotLight
  Enabled = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Visible = True
  PixelsPerInch = 96
  TextHeight = 13
  object Lista_jogos: TListBox
    Left = 360
    Top = 40
    Width = 313
    Height = 313
    ItemHeight = 13
    TabOrder = 0
    OnClick = Lista_jogosClick
  end
  object bt_inserir: TButton
    Left = 56
    Top = 136
    Width = 75
    Height = 25
    Caption = 'Inserir'
    TabOrder = 1
    OnClick = bt_inserirClick
  end
  object bt_remover: TButton
    Left = 137
    Top = 136
    Width = 82
    Height = 25
    Caption = 'Remover'
    TabOrder = 2
    OnClick = bt_removerClick
  end
  object bt_atualizar: TButton
    Left = 56
    Top = 167
    Width = 75
    Height = 25
    Caption = 'Atualizar'
    Enabled = False
    TabOrder = 3
    OnClick = bt_atualizarClick
  end
  object bt_salvar: TButton
    Left = 225
    Top = 136
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 4
    OnClick = bt_salvarClick
  end
  object bt_carregar: TButton
    Left = 137
    Top = 167
    Width = 82
    Height = 25
    Caption = 'Carregar '
    TabOrder = 5
    OnClick = bt_carregarClick
  end
  object Nome: TEdit
    Left = 56
    Top = 40
    Width = 244
    Height = 21
    TabOrder = 6
  end
  object Ano: TEdit
    Left = 56
    Top = 67
    Width = 244
    Height = 21
    TabOrder = 7
  end
  object Genero: TEdit
    Left = 56
    Top = 94
    Width = 244
    Height = 21
    TabOrder = 8
  end
  object bt_limpar: TButton
    Left = 225
    Top = 167
    Width = 75
    Height = 25
    Caption = 'Limpar'
    TabOrder = 9
    OnClick = bt_limparClick
  end
end
