unit ProjetoT;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Lista_jogos: TListBox;
    bt_inserir: TButton;
    bt_remover: TButton;
    bt_atualizar: TButton;
    bt_salvar: TButton;
    bt_carregar: TButton;
    Nome: TEdit;
    Genero: TEdit;
    Ano: TEdit;
    bt_limpar: TButton;
    procedure bt_salvarClick(Sender: TObject);
    procedure Lista_jogosClick(Sender: TObject);
    procedure bt_atualizarClick(Sender: TObject);
    procedure bt_limparClick(Sender: TObject);
    procedure bt_removerClick(Sender: TObject);
    procedure bt_inserirClick(Sender: TObject);
    procedure bt_carregarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.bt_atualizarClick(Sender: TObject);
begin
Lista_jogos.DeleteSelected;
Lista_jogos.Items[Lista_jogos.ItemIndex] := (Nome.Text);
Lista_jogos.Items[Lista_jogos.ItemIndex] := (Ano.Text);
Lista_jogos.Items[Lista_jogos.ItemIndex] := (Genero.Text);

end;

procedure TForm1.bt_salvarClick(Sender: TObject);
var
arquivo: TextFile;
  i: Integer;
begin
AssignFile (arquivo, 'Jogos.txt');
Rewrite (arquivo);
begin for i := 0 to Lista_jogos.Items.Count-1 do
Writeln (arquivo, Lista_jogos.Items.Strings[i]);
end;
CloseFile (arquivo);
 end;

procedure TForm1.bt_carregarClick(Sender: TObject);
begin
Lista_Jogos.Items.LoadFromFile('Jogos.txt');
end;

procedure TForm1.bt_inserirClick(Sender: TObject);
begin
Lista_Jogos.Items.Add(Nome.Text);
Nome.Clear;
Lista_Jogos.Items.Add(Ano.Text);
Ano.Clear;
Lista_Jogos.Items.Add(Genero.Text);
Genero.Clear;
end;

procedure TForm1.bt_limparClick(Sender: TObject);
begin
Lista_Jogos.Clear;
end;

procedure TForm1.bt_removerClick(Sender: TObject);
begin
Lista_Jogos.DeleteSelected;
end;

procedure TForm1.Lista_jogosClick(Sender: TObject);
begin
if Lista_jogos.ItemIndex > -1 then
begin
  bt_atualizar.Enabled:= true;
end;
end;

end.
