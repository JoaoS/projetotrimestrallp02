object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 521
  ClientWidth = 779
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ListaPersonagens: TListBox
    Left = 546
    Top = 8
    Width = 217
    Height = 473
    ItemHeight = 13
    TabOrder = 0
    OnClick = ListaPersonagensClick
  end
  object edit: TEdit
    Left = 16
    Top = 8
    Width = 377
    Height = 21
    TabOrder = 1
  end
  object btinserir: TButton
    Left = 16
    Top = 48
    Width = 75
    Height = 25
    Caption = 'inserir'
    TabOrder = 2
    OnClick = btinserirClick
  end
  object BtExcluir: TButton
    Left = 112
    Top = 48
    Width = 75
    Height = 25
    Caption = 'excluir'
    TabOrder = 3
    OnClick = BtExcluirClick
  end
  object BtLimpar: TButton
    Left = 208
    Top = 48
    Width = 75
    Height = 25
    Caption = 'limpar'
    TabOrder = 4
    OnClick = BtLimparClick
  end
  object btAtualizar: TButton
    Left = 297
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Atualizar'
    Enabled = False
    TabOrder = 5
    OnClick = btAtualizarClick
  end
  object BTS: TButton
    Left = 16
    Top = 96
    Width = 356
    Height = 25
    Caption = 'salvar em arquivo'
    TabOrder = 6
    OnClick = BTSClick
  end
end
